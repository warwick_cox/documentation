var Core = Core || {
    
    init: function() {
        this.setOption();
        this.changeOption();
        this.determineSection();
        this.setMenuFocus();
        this.demoForm();
    },

    setOption: function() {
        var _this = this;
        if(localStorage.getItem('option') == null) {
            var selectedOption = $('.options_header .option.active').data('option');
            if(selectedOption != undefined) {
                localStorage.setItem('option', selectedOption);
                console.log('should set one to localStorage');
            } else {
                _this.selectFirstOption();
                console.log('its selecting the first one');
            }
        } else {
            var setOption = localStorage.getItem('option');
            _this.displayOption(setOption);
        }
    },

    displayOption: function(setOption) {
        $('.options_header .option').removeClass('active');
        $('.option_content').removeClass('active');
        $('.options_header .option[data-option="'+setOption+'"]').addClass('active');
        $('.option_content[data-option="'+setOption+'"]').addClass('active');
    },

    selectFirstOption: function() {
        // select the first one, and set the localStorage
        var firstOption = $('.options_header .option').first();
        firstOption.addClass('active');
        localStorage.setItem('option', firstOption.data('option'));
    },

    changeOption: function() {
        var _this = this;
        $('.options_header .option').on('click', function() {
            var option = $(this).data('option');
            _this.displayOption(option);
            localStorage.setItem('option', option);
        });
    },

    determineSection: function() {
        var sections = $('.sections a');
        $.each(sections, function(i, section) {
            if($(section).attr('href') == window.location.pathname) {
                $(section).addClass('active');
            }
        });
    },

    isElementVisible: function(elementToBeChecked) {
        var TopView = $(window).scrollTop();
        var BotView = TopView + $(window).height();
        var TopElement = $(elementToBeChecked).offset().top;
        var BotElement = TopElement + $(elementToBeChecked).height();
        return ((BotElement <= BotView) && (TopElement >= TopView));
    },

    checkIfContentIsVisible: function() {
        var _this = this;
        var content_section = $('.content_section .documentation_header');
        $.each(content_section, function(i, section) {
            var id = $(section).attr('id');
            if(Core.isElementVisible('.content_section .documentation_header#'+id)) {
                $('.sidebar-nav-items a').removeClass('active');
                $('.sidebar-nav-items a[href="#'+id+'"]').addClass('active');
            }
        });
    },

    setMenuFocus: function() {
        var _this = this;
        var timer = 0;
        $(window).scroll(function() {
            function checkNow () {            
                _this.checkIfContentIsVisible();
            }
            if (timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(checkNow, 300); 
        });
    },

    demoForm: function() {
        $('form.demo .submit_demo').click(function() {
            
            var form = $(this).closest("form.demo");
            var method = form.data('method');
            var isjson = form.data('isjson');
            var endpoint = form.attr('action');
            var data = '';
            // console.log(method);
            // console.log(isjson);
            // console.log(endpoint);

            if(isjson) {
                data = {};
                $(form).find('input[type="text"]').each(function(index,input) {
                    var name = $(input).attr('name');
                    var value = $(input).val();
                    data[name] = value
                });
                data = JSON.stringify( data );
                data = {json: data};
                console.log(data);
            } else {
                var data = $(form).serialize();
                console.log(data);
            }
            $.ajax({
                type: method,
                url: endpoint,
                data: data,
                dataType: 'text json',
                success: function(responseData, textStatus, jqXHR) {
                    console.log(responseData);
                },
                error: function (responseData, textStatus, errorThrown) {
                    console.log(responseData);
                }
            });


        });
    }

}


















