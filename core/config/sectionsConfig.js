
/**
*
*   @sections = These are the tab options available on the top left. Add / Remove sections to display them.
*   A section allows you to have multiple pages on your documentation site that are all interlinked.
*
**/

var sections = [
    {
        name: 'home',
        url: 'https://crowd.delivery'
    },
    {
        name: 'API',
        url: '/'
    }
];

module.exports.sections = sections;
