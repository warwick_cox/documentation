
/**
*
*   @content order = Order the objects in which ever order you want them displayed. First object will display at the top of the page, etc.
*   @Menu = The menu is generated from the `name` item in each object
*   @file types = .md (Markdown) or .html are accepted
*
*   @name = The name in the menu, this is used as a ancor. So must be unique.
*   @content = This is the path to the content markDown file
*   @options = Each option must be specified in the optionsConfig.js file in order for the content to display.
*   
Example OBJECT

    {
        name: "Introduction",
        content: "src/content/API/Introduction/intro.md",
        php: "src/content/API/Introduction/intro-PHP.md",
        js: "src/content/API/Introduction/intro-JS.md",
        demo: "src/content/API/Introduction/intro-demo.html"
    },

**/

var content = [
    {
        name: "Introduction",
        content: "src/content/API/Introduction/intro.md",
        php: "src/content/API/Introduction/intro-PHP.md",
        js: "src/content/API/Introduction/intro-JS.md",
        demo: "demoFormsConfig"
    },
    {
        name: "Orders",
        content: "src/content/API/Orders/orders.md",
        php: "src/content/API/Orders/orders-PHP.md",
        js: "src/content/API/Orders/orders-JS.md",
        demo: "demoFormsConfig"
    }
];

module.exports.content = content;
