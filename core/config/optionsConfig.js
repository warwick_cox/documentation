
/**
*
*   @Options = These are the tab options available on the top right. Add / Remove options to display them.
*   Each option is NOT REQUIRED in each contentConfig object.
*
**/

var options = ['php', 'js', 'java', 'android', 'json', 'demo'];

module.exports.options = options;
