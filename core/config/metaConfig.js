
/**
*
*   @content order = Order the objects in which ever order you want them displayed. First object will display at the top of the page, etc.
*   @Menu = The menu is generated from the `name` item in each object
*   @file types = .md (Markdown) or .html are accepted
*
*   @name = The name in the menu, this is used as a ancor. So must be unique.
*   @content = This is the path to the content markDown file
*   @options = Each option must be specified in the optionsConfig.js file in order for the content to display.
*   
Example OBJECT


**/

var meta = [
    {
        title: "Create Documentation",
        description: "Welcome to Create Documentation"
    }
];

module.exports.meta = meta;
