var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var browserSync = require('browser-sync').create();
var minifyHTML = require('gulp-minify-html');
var replace = require('gulp-replace');
var del = require('del');
var markdown = require('gulp-markdown');
var less = require('gulp-less');
var path = require('path');
var fs = require('fs');
var insert = require('gulp-insert');
var inject = require('gulp-inject-string');
var addsrc = require('gulp-add-src');
var file = require('gulp-file');
var runSequence = require('run-sequence');

// Local config files
var contentConfig = require('./core/config/contentConfig.js');
var optionsConfig = require('./core/config/optionsConfig.js');
var sectionsConfig = require('./core/config/sectionsConfig.js');
var metaConfig = require('./core/config/metaConfig.js');
var demoFormsConfig = require('./core/config/demoFormsConfig.json');

/*

////////////////////
//  DEVELOPMENT:  //
////////////////////

gulp watch



///////////////////
//  PRODUCTION:  //
///////////////////

----- FIRST ------
gulp codeReview 

----- SECOND -----
gulp build 
*/ 



gulp.task('default', function () {
    return runSequence('removeTempFiles', 'buildPartials', 'less', 'images', 'js', 'buildIndex');
});

gulp.task('removeTempFiles', function () {
  return del(['src/temp/*']);
});

gulp.task('buildContent', function () {
    var i = 10000;
    return Object.keys(contentConfig).forEach(function(objKey) {
        var configObj = contentConfig[objKey];
        Object.keys(configObj).forEach(function(key) {
            var obj = configObj[key];
            var tempFolder = 'src/temp/content';
            var contentStart = '<div class="row content_section">'+
                        '<div class="large-2 columns menu_placeholder clear_text">menu</div>'+
                        '<div class="large-5 columns documentation_header" id="'+obj.name+'">';
            var contentEndOptionStart = '</div><div class="large-5 columns options_display">';
            var closeDiv = '</div>';
            var optionEnd = '</div></div> <div class="row content_section hr content_divider"><div class="large-2 columns horizontal_line clear_text">--</div><div class="large-5 columns horizontal_line clear_text">--</div><div class="large-5 columns options_display horizontal_line clear_text">--</div></div>';

            gulp.src(obj.content)
                .pipe(markdown())
                .pipe(inject.prepend(contentStart))
                .pipe(concat('html_'+i+'.html'))
                .pipe(gulp.dest(tempFolder));
            i = ++i;

            file('html_'+i+'.html', contentEndOptionStart, { src: true }).pipe(gulp.dest(tempFolder));
            i = ++i;

            // console.log(optionsConfig.options);
            var options = optionsConfig.options;
            for (var o = options.length - 1; o >= 0; o--) {
                file('html_'+i+'.html', '<div class="option_content" data-option="'+options[o]+'">', { src: true }).pipe(gulp.dest(tempFolder));
                i = ++i;
                if(obj[options[o]] == 'demoFormsConfig') {
                    buildDemoForm(i, obj.name, tempFolder);
                    
                    i = ++i;
                } else if(obj[options[o]] != undefined) {
                    gulp.src(obj[options[o]])
                        .pipe(markdown())
                        .pipe(concat('html_'+i+'.html'))
                        .pipe(gulp.dest(tempFolder));
                    i = ++i;
                }

                file('html_'+i+'.html', closeDiv, { src: true }).pipe(gulp.dest(tempFolder));
                i = ++i;
            };

            file('html_'+i+'.html', optionEnd, { src: true }).pipe(gulp.dest(tempFolder));
            i = ++i;

        });
    });
});

gulp.task('buildMenu', function () {
    var i = 10000;
    return Object.keys(contentConfig).forEach(function(objKey) {
        var configObj = contentConfig[objKey];
        Object.keys(configObj).forEach(function(key) {
            var obj = configObj[key];
            var menuItem = '<li><a class="sidebar-nav-item" href="#'+obj.name+'">'+obj.name+'</a></li>';
            file('menu_'+i+'.html', menuItem, { src: true }).pipe(gulp.dest('src/temp/menu'));
            i = ++i;

        });
    });
});

gulp.task('buildOptions', function () {
    var i = 10000;
    return Object.keys(optionsConfig.options).forEach(function(objKey) {
        var option = optionsConfig.options[objKey];
        var optionItem = '<div class="option" data-option="'+option+'">'+option+'</div>';
        file('option_'+i+'.html', optionItem, { src: true }).pipe(gulp.dest('src/temp/options'));
        i = ++i;
    });
});

gulp.task('buildMeta', function () {
    var i = 10000;
    return Object.keys(metaConfig.meta).forEach(function(objKey) {
        var metaData = metaConfig.meta[objKey];
        var meta = '<title>'+metaData.title+'</title><meta name="description" content="'+metaData.description+'"/>';
        file('meta_'+i+'.html', meta, { src: true }).pipe(gulp.dest('src/temp/meta'));
        i = ++i;
    });
});

gulp.task('buildSections', function () {
    var i = 10000;
    return Object.keys(sectionsConfig.sections).forEach(function(objKey) {
        var section = sectionsConfig.sections[objKey];
        var sectionItem = '<a href="'+section.url+'" class="">'+section.name+'</a>';
        file('sections_'+i+'.html', sectionItem, { src: true }).pipe(gulp.dest('src/temp/sections'));
        i = ++i;
    });
});

gulp.task('buildPartials', ['buildContent', 'buildMenu', 'buildSections', 'buildOptions', 'buildMeta'], function() {});


gulp.task('buildIndex', function () {
  return gulp.src( ['core/includes/1headStart.html',
                    'src/temp/meta/*.html', 
                    'core/includes/1headEnd.html',
                    'core/includes/2preSections.html',
                    'src/temp/sections/*.html', 
                    'core/includes/3postSections.html',
                    'core/includes/4preMenu.html',
                    'src/temp/menu/*.html', 
                    'core/includes/5postMenu.html',
                    'core/includes/6preOptions.html',
                    'src/temp/options/*.html', 
                    'core/includes/7postOptions.html',
                    'core/includes/8preContent.html',
                    'src/temp/content/*.html',
                    'core/includes/9footer.html'])
    .pipe(concat('index.html'))
    .pipe(gulp.dest('web'));
});

gulp.task('build', function() {
    runSequence('removeTempFiles', 'buildPartials', 'less', 'images', 'js', 'buildIndex');
});

gulp.task('watch', ['less', 'images', 'js', 'buildIndex'], function() {
    gulp.watch(["src/less/**/*.less", "core/less/**/*.less"], ['less']);
    gulp.watch("src/images/*", ['images']);
    gulp.watch(["src/js/*.js", "core/js/*.js"], ['js']);
    gulp.watch("src/content/**/*.*", ['buildIndex']);
});

gulp.task('less', function () {
  return gulp.src(['src/less/**/*.less','core/less/*.less'])
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('web/css'));
});

gulp.task('images', function() {
    return gulp.src("src/images/*")
        .pipe(gulp.dest("web/images"));
});

gulp.task('js', function() {
    return gulp.src(["src/js/**/*.js", "core/js/**/*.js"])
        .pipe(gulp.dest("web/js"));
});

function buildDemoForm(i, contentName, tempFolder) {

    function clearRow() {
        return  '<div class="row"><div class="large-6 columns clear_text">-</div><div class="large-4 columns clear_text">-</div><div class="large-2 columns clear_text">-</div></div>';
    }
    function topRow (x, y, z) {
        return  '<div class="row">'+
                    '<div class="large-6 columns text-right ">'+x+' : </div>'+
                    '<div class="large-4 columns hljs-keyword '+z+'">'+y+'</div>'+
                    '<div class="large-2 columns clear_text">-</div>'+
                '</div>';
    }
    function inputRow (x) {
        return  '<div class="row">'+
                    '<div class="large-6 columns text-right hljs-string">'+x+' : </div>'+
                    '<div class="large-4 columns"><input name="'+x+'" class="demoInput" type="text" placeholder="Enter '+x+'"/></div>'+
                    '<div class="large-2 columns clear_text">-</div>'+
                '</div>';
    }
    function buttonFormEnd (name) {
        return  '<div class="row">'+
                    '<div class="large-2 columns clear_text">-</div>'+
                    '<div class="large-8 columns text-center">'+
                        '<a href="#'+name+'" class="button small expand submit_demo">Test</a>'+
                    '</div>'+
                    '<div class="large-2 columns clear_text">-</div>'+
                '</div>'+
                '</code>'+
            '</form>';

    }
    function formBeginning (endpoint, requestMethod, isJson) {
        return '<form class="demo" action="'+endpoint+'" data-method="'+requestMethod+'" data-isjson="'+isJson+'">';

    }
    Object.keys(demoFormsConfig).forEach(function(key) {
        var formData = demoFormsConfig[key];
        var form = '';
        var formStart = '';
        if(formData.name === contentName) {
            form = form + '<code class="lang-javascript hljs">';
            form = form + topRow('HTTP Request Method', formData.requestMethod, 'requestMethod');
            form = form + topRow('Endpoint', formData.endpoint, 'endpoint');

            // Get all the parameters
            Object.keys(formData.parameters).forEach(function(key) {
                var parameters = formData.parameters[key];
                if(parameters.input_type == 'text') {
                    form = form + clearRow();
                    form = form + inputRow(parameters.name);
                    formStart = formStart + formBeginning(formData.endpoint, formData.requestMethod, 'false');
                } else {
                    form = form + topRow('Parameter', parameters.name, 'isjson');
                    form = form + clearRow();
                    formStart = formStart + formBeginning(formData.endpoint, formData.requestMethod, 'true');

                    if(parameters.options != undefined) {
                        Object.keys(parameters.options).forEach(function(key) {
                            var options = parameters.options[key];
                            form = form + inputRow(options.name);

                        });
                    }
                    
                }
            });

            form = formStart + form + buttonFormEnd(formData.name);
            file('html_'+i+'.html', form, { src: true }).pipe(gulp.dest(tempFolder));
        }
    });
}


























