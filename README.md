# DOCUMENTATION Awesomeness #

Welcome to the documentation site that is built in a way for you to skin your own site.

### Getting started ###

* Clone the repo
```
git clone git@bitbucket.org:warwick_cox/documentation.git
```

* Run up MAMP / or whatever you use to create host names. Maybe even your hosts file. (optional)

* Install npm - [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
* Install gulp - [Gulp getting started](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
```console
$ npm install --global gulp
```
```console
$ npm install --save-dev gulp
```


### Building your documentation ###

* Create folders of content in the src folder (No convention is needed)

* Use Markdown to create you documentation

| Reference | link |
|---|---|
| Markdown cheat sheet | [Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)|
| code highlighting | [syntax-highlighting-in-markdown](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown)








Copyright (c) 2015 Warwick Cox

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.